//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res,next){
 res.header("Access-Control-Allow-Origin","*");
 res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
 next();
});

var requestjson = require('request-json');
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca4mb79734/collections/movimientos?apiKey=t8ghbjSKSMlr9zKDGamhJsHAYLUX9H-S";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  //res.send('hola mundo');
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get("/clientes/:idcliente",function(req,res){
  //res.send('hola mundo');
  res.send('cliente numero: '+req.params.idcliente);
});

app.post("/",function(req,res){
  res.send('hemos recibido su peticion cambiada');
});
app.put("/",function(req,res){
  res.send('hemos recibido su la actualizacion');
});
app.delete("/",function(req,res){
  res.send('hemos eliminado su registro');
});


//****************************************************************
/*
1.- crer el post con id cliente nombre y apellido
2.- gestor-clientes en vez de nwind a nuestros movimientos
3.- crear la imagen de ambas aplicaciones en front en el 3001 y el back en 3002
*/
app.get("/movimientos",function(req,res){
  clienteMlab.get ('',function (err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  });
});

app.post("/movimientos",function(req,res){
  clienteMlab.post('',req.body, function(err, resM, body){
    res.send(body);
  });
});
